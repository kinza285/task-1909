namespace :data do
  desc "Populate DB with users and answers"
  task :populate => :environment do
    for i in 1..100000 do
      user = User.create(name: Faker::Name.unique.name, timezone: Faker::Address.time_zone)
      for j in 1..1000 do
        Answer.create(user_id: user.id, correct: Faker::Boolean.boolean, created_at: Faker::Time.backward(days: 183))
      end
    end
    # TODO: 100,000 users with 1,000 answers each. See TASKS.md
  end
end
