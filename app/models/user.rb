class User < ApplicationRecord
  has_many :answers
  def corrected_percentage(start_date, end_date)
    answers = self.answers.where("created_at  BETWEEN ? AND ?", start_date, end_date)
    (100.0 * answers.correct.count / answers.count ).round
  end
end
