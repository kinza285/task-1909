class Answer < ApplicationRecord
  scope :correct, -> { where(correct: true) }
end
