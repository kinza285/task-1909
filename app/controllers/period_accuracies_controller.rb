class PeriodAccuraciesController < ApplicationController
  def new
    @period_accuracy = PeriodAccuracy.new
  end
  def create
    @user = User.find_by(name: params[:period_accuracy][:name])
    Time.use_zone(@user.timezone) do
      @start_date = Time.zone.parse(params[:period_accuracy][:start_date].to_s)
      @end_date = Time.zone.parse(params[:period_accuracy][:end_date].to_s)
    end
    @answers = @user.answers
    @answers_in_period = @answers.where("created_at  BETWEEN ? AND ?", @start_date, @end_date)
    @correct = @answers.correct
    @correct_in_period = @correct.where("created_at  BETWEEN ? AND ?", @start_date, @end_date)
    @percentage_total = 100.0 * @correct.count / @answers.count
    @percentage__in_period = if @answers_in_period
                                100.0 * @correct_in_period.count / @answers_in_period.count
                             else
                                 'No answers'
                             end    
  end
end