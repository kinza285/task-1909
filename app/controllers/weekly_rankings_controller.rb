class WeeklyRankingsController < ApplicationController
  def new
    @weekly_ranking = WeeklyRanking.new
  end
  def create
    @start_date = params[:weekly_ranking][:start_date].to_date.beginning_of_week
    @start_date_time = Time.zone.parse(@start_date.to_s).utc
    @end_date = params[:weekly_ranking][:start_date].to_date.end_of_week + 1
    @end_date_time = Time.zone.parse(@end_date.to_s).utc
    @prev_start_date = @start_date - 7
    @prev_start_date_time = Time.zone.parse(@prev_start_date.to_s).utc
    @prev_end_date = @end_date - 7
    @prev_end_date_time = Time.zone.parse(@prev_end_date.to_s).utc
    @users = User.all.sort_by{|user| user.corrected_percentage(@start_date_time, @end_date_time)}.first(100)
    @ids = User.all.sort_by{|user| user.corrected_percentage(@prev_start_date_time, @prev_end_date_time)}.map(&:id)
  end
end