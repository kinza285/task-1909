Rails.application.routes.draw do
  root to: 'home#index'
  resource :period_accuracies, only: [:new, :create]
  resource :weekly_rankings, only: [:new, :create]
end
